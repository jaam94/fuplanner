## Used cookies

s_session_id
session_id

## Get user profile and ID

https://lms.fu-berlin.de/learn/api/v1/users/?userName=seedorf96
https://lms.fu-berlin.de/learn/api/v1/users/_203980_1

## Course list

https://lms.fu-berlin.de/learn/api/public/v1/users/_203980_1/courses

## Course details

https://lms.fu-berlin.de/learn/api/public/v1/courses/_122802_1

The field "courseId" in Blackboard can be matched to "props/kvv_lvnumbers" in "http://mycampus.imp.fu-berlin.de/direct/site/<SITE_ID>"

 - details
     + semester
         * type: parse "courseId"
         * year: parse "courseId"
     + lvNumber
         * parse "courseId"
     + title
         * "name"
     + lecturer
         * https://lms.fu-berlin.de/learn/api/public/v1/courses/_127131_1/users
         * Find Instructor
         * Resolve "userId" to name
     + type
         * parse "courseId" ('Vorlesung'/'Seminar'/...?)
     + description
         * ""
     + ID
         * id

## Course annoncements

https://lms.fu-berlin.de/learn/api/v1/courses/_127131_1/announcements?limit=1000&offset=0

 - catch 400 (tool is not available)
 - details
     + id
         * "id"
     + title
         * "title"
     + body
         * "body/rawText"
     + createdBy
         * "creatorUserId"
         * search user name by id
     + createdOn
         * "startDateRestriction"
     + urls
         * not available / empty

## Course resources

https://lms.fu-berlin.de/learn/api/public/v1/courses/_127131_1/contents

 - details
     + author
         * folder: N/A
         * file: N/A
     + modifiedDate
         * folder: "created"
         * file: "created"
     + title
         * folder: "title"
         * file: "title"
     + url
         * folder: N/A
         * file: N/A
     + visible
         * folder: "availability/available"
         * file: "availability/available"
     + container
         * folder: "folder" ? same as kvv
         * file: "file" ? same as kvv
 - Folder "resource/x-bb-folder"
     + https://lms.fu-berlin.de/learn/api/public/v1/courses/_127131_1/contents/_3602247_1/children
 - File "resource/x-bb-document"
     + https://lms.fu-berlin.de/learn/api/public/v1/courses/_127131_1/contents/_3635282_1/attachments
     + text: "body"
 - Attachment-Download
     + https://lms.fu-berlin.de/learn/api/public/v1/courses/_127131_1/contents/_3635282_1/attachments/_2838023_1/download
