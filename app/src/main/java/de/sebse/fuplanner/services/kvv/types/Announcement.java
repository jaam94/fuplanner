package de.sebse.fuplanner.services.kvv.types;

import com.google.android.gms.common.internal.Objects;

import java.io.Serializable;
import java.util.ArrayList;

import androidx.annotation.NonNull;

public class Announcement implements Serializable {
    private final String id;
    private final String title;
    private final String body;
    private final String createdBy;
    private final long createdOn;
    private final ArrayList<String> urls;

    public Announcement(String id, String title, String body, String createdBy, long createdOn, ArrayList<String> urls) {
        this.id = id;
        this.title = title;
        this.body = body;
        this.createdBy = createdBy;
        this.createdOn = createdOn;
        this.urls = urls;
    }

    public ArrayList<String> getUrls() {
        return urls;
    }

    public String getId() {
        return id;
    }

    public String getTitle() {
        return title;
    }

    public String getBody() {
        return body;
    }

    public String getCreatedBy() {
        return createdBy;
    }

    public long getCreatedOn() {
        return createdOn;
    }

    @NonNull
    @Override
    public String toString() {
        return "ID: "+getId()+
                "\nTitle: "+getTitle()+
                "\nBody length: "+getBody().length()+
                "\nCreated by: "+getCreatedBy()+
                "\nCreated on: "+getCreatedOn()+
                "\nURLs: "+getUrls().toString();
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId(), getBody(), getCreatedBy(), getCreatedOn(), getTitle(), getUrls());
    }
}
