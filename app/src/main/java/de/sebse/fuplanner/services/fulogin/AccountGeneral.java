package de.sebse.fuplanner.services.fulogin;

public class AccountGeneral {
    public static final String ACCOUNT_TYPE = "de.sebse.fuplanner.fuauth";
    public static final String AUTHTOKEN_TYPE_KVV = "KVV";
    public static final String AUTHTOKEN_TYPE_BLACKBOARD = "Blackboard";
}
