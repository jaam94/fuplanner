package de.sebse.fuplanner.services.canteen.types;

public interface CanteenListener {
    void onCanteenRefreshCompleted(boolean b);
}
