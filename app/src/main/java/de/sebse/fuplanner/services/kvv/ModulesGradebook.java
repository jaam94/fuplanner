package de.sebse.fuplanner.services.kvv;

import android.content.Context;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import de.sebse.fuplanner.services.kvv.types.Grade;
import de.sebse.fuplanner.services.kvv.types.Gradebook;
import de.sebse.fuplanner.services.kvv.types.Modules;
import de.sebse.fuplanner.tools.network.NetworkCallback;
import de.sebse.fuplanner.tools.network.NetworkError;
import de.sebse.fuplanner.tools.network.NetworkErrorCallback;

public class ModulesGradebook extends PartModules<Gradebook> {

    ModulesGradebook(Login login, ModulesList list, Context context) {
        super(login, list, context);
    }

    @Override
    protected Gradebook getPart(Modules.Module module) {
        return module.gradebook;
    }

    @Override
    protected boolean setPart(Modules.Module module, Gradebook part) {
        boolean changed = module.gradebook == null || module.gradebook.hashCode() != part.hashCode();
        module.gradebook = part;
        return changed;
    }

    @Override
    protected void upgradeKVV(final String ID, final NetworkCallback<Gradebook> callback, final NetworkErrorCallback errorCallback) {
        if (!mLogin.isInOnlineMode() || mLogin.getLoginTokenKVV() == null || !mLogin.getLoginTokenKVV().isAvailable()) {
            errorCallback.onError(new NetworkError(101504, 500, "Currently running in offline mode!"));
            return;
        }
        super.get(String.format(Constants.KVV_SERVER_URL+"direct/gradebook/site/%s.json", ID), mLogin.getLoginTokenKVV().getCookies(), response -> {
            String body = response.getParsed();
            if (body == null) {
                errorCallback.onError(new NetworkError(101501, 403, "No gradebook retrieved!"));
                return;
            }
            Gradebook gradebook = new Gradebook();
            JSONArray sites;
            try {
                JSONObject json = new JSONObject(body);
                sites = json.getJSONArray("assignments");
            } catch (JSONException e) {
                e.printStackTrace();
                errorCallback.onError(new NetworkError(101502, 403, "Cannot parse gradebook!"));
                return;
            }

            for (int i = 0; i < sites.length(); i++) {
                try {
                    JSONObject site = sites.getJSONObject(i);
                    double grade = site.optDouble("grade", 0);
                    String itemName = site.optString("itemName", null);
                    double maxPoints = site.optDouble("points", -1);

                    gradebook.addGrade(new Grade(itemName, grade, maxPoints));
                } catch (JSONException e) {
                    log.e(new NetworkError(101505, 403, "Cannot parse gradebook!"));
                    log.e("ID:", i, "JSON:", sites);
                    e.printStackTrace();
                    return;
                }
            }
            callback.onResponse(gradebook);
        }, error -> {
            if (error.networkResponse.statusCode == 400)
                callback.onResponse(new Gradebook());
            else
                errorCallback.onError(new NetworkError(101503, error.networkResponse.statusCode, "Cannot get gradebook!"));
        });
    }

    @Override
    protected void upgradeBB(String ID, NetworkCallback<Gradebook> callback, NetworkErrorCallback errorCallback) {
        if (!mLogin.isInOnlineMode() || mLogin.getLoginTokenBB() == null || !mLogin.getLoginTokenBB().isAvailable()) {
            errorCallback.onError(new NetworkError(101510, 500, "Currently running in offline mode!"));
            return;
        }
        super.get(String.format("https://lms.fu-berlin.de/learn/api/public/v2/courses/%s/gradebook/columns", ID), mLogin.getLoginTokenBB().getCookies(), response -> {
            String body = response.getParsed();
            if (body == null) {
                errorCallback.onError(new NetworkError(101511, 400, "No gradebook columns retrieved!"));
                return;
            }
            JSONArray gradeColumns;
            try {
                JSONObject json = new JSONObject(body);
                gradeColumns = json.getJSONArray("results");
            } catch (JSONException e) {
                e.printStackTrace();
                errorCallback.onError(new NetworkError(101512, 400, "Cannot parse gradebook columns!"));
                return;
            }

            get(String.format("https://lms.fu-berlin.de/learn/api/public/v2/courses/%s/gradebook/users/%s", ID, mLogin.getLoginTokenBB().getId()), mLogin.getLoginTokenBB().getCookies(), response1 -> {
                String body1 = response1.getParsed();
                if (body1 == null) {
                    errorCallback.onError(new NetworkError(101513, 400, "No gradebook entries retrieved!"));
                    return;
                }
                JSONArray grades;
                try {
                    JSONObject json = new JSONObject(body1);
                    grades = json.getJSONArray("results");
                } catch (JSONException e) {
                    e.printStackTrace();
                    errorCallback.onError(new NetworkError(101514, 400, "Cannot parse gradebook entries!"));
                    return;
                }

                Gradebook result = new Gradebook();
                for (int i = 0; i < grades.length(); i++) {
                    for (int j = 0; j < gradeColumns.length(); j++) {
                        try {
                            JSONObject grade = grades.getJSONObject(i);
                            JSONObject column = gradeColumns.getJSONObject(j);
                            String grade_id = grade.getString("columnId");
                            String column_id = column.getString("id");
                            if (!grade_id.equals(column_id))
                                continue;
                            String name = column.getString("name");
                            JSONObject displayGrade = grade.optJSONObject("displayGrade");
                            double points = displayGrade != null ? displayGrade.optDouble("score", 0) : 0;
                            JSONObject score = column.optJSONObject("score");
                            double maxPoints = score != null ? score.optDouble("possible", 0) : 0;

                            result.addGrade(new Grade(name, points, maxPoints));
                        } catch (JSONException e) {
                            log.e(new NetworkError(101515, 400, "Cannot parse grades!"));
                            log.e("ID:", i, "JSON-grades:", grades);
                            log.e("ID:", j, "JSON-gradeColumns:", gradeColumns);
                            e.printStackTrace();
                            return;
                        }
                    }
                }
                callback.onResponse(result);
            }, error -> errorCallback.onError(new NetworkError(101516, error.networkResponse.statusCode, "Cannot get gradebook columns!")));
        }, error -> {
            if (error.networkResponse.statusCode == 403)
                callback.onResponse(new Gradebook());
            else
                errorCallback.onError(new NetworkError(101517, error.networkResponse.statusCode, "Cannot get gradebook entries!"));
        });
    }
}
