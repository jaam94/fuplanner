package de.sebse.fuplanner.services.kvv.sync;

import android.app.Service;
import android.content.Intent;
import android.os.IBinder;

public class KVVSyncService extends Service {

    private static final Object sSyncAdapterLock = new Object();
    private static KVVSyncAdapter sSyncAdapter = null;

    @Override
    public void onCreate() {
        synchronized (sSyncAdapterLock) {
            if (sSyncAdapter == null)
                sSyncAdapter = new KVVSyncAdapter(getApplicationContext(), true);
        }
    }

    @Override
    public IBinder onBind(Intent intent) {
        return sSyncAdapter.getSyncAdapterBinder();
    }
}
