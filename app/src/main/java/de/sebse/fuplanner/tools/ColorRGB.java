package de.sebse.fuplanner.tools;

import android.graphics.Paint;

public class ColorRGB implements Color {
    private final int mRed;
    private final int mGreen;
    private final int mBlue;

    private ColorRGB(int red, int green, int blue) {
        this.mRed = red;
        this.mGreen = green;
        this.mBlue = blue;
    }

    public ColorRGB(double red, double green, double blue) {
        this((int) (red*255), (int) (green*255), (int) (blue*255));
    }

    @Override
    public void setPaintColor(Paint paint) {
        paint.setARGB(255, mRed, mGreen, mBlue);
    }
}
