package de.sebse.fuplanner.tools.ui.treeview;

/**
 * Created by tlh on 2016/10/1 :)
 */

public interface LayoutItemType {
    int getLayoutId();
}
