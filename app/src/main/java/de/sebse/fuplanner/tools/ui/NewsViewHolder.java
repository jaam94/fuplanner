package de.sebse.fuplanner.tools.ui;

import android.view.View;
import android.widget.TextView;

import androidx.annotation.NonNull;
import de.sebse.fuplanner.R;

public class NewsViewHolder extends CustomViewHolder {
    public final TextView mHeader;
    public final TextView mSubLeft;
    public final TextView mSubRight;
    public final TextView mText;

    public NewsViewHolder(View view) {
        super(view);
        mHeader = view.findViewById(R.id.header);
        mSubLeft = view.findViewById(R.id.sub_left);
        mSubRight = view.findViewById(R.id.sub_right);
        mText = view.findViewById(R.id.text);
    }

    @NonNull
    @Override
    public String toString() {
        return super.toString() + " '" + mHeader.getText() + "' '" + mSubLeft.getText() + "' '" + mSubRight.getText() + "' '" + mText.getText() + "'";
    }
}
