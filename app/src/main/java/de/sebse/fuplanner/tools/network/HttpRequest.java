package de.sebse.fuplanner.tools.network;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.toolbox.HttpHeaderParser;

/**
 * Created by sebastian on 24.10.17.
 */

class HttpRequest extends Request<Result> {
    private static final int MY_SOCKET_TIMEOUT_MS = 15000;
    private final Response.Listener<Result> mListener;

    public HttpRequest(int method, String url, Response.Listener<Result> listener,
                       Response.ErrorListener errorListener) {
        super(method, url, errorListener);
        mListener = listener;
        this.setRetryPolicy(new DefaultRetryPolicy(
                MY_SOCKET_TIMEOUT_MS,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
    }

    @Override
    protected Response<Result> parseNetworkResponse(NetworkResponse response) {
        Result result = new Result(response.data, response.headers);

        return Response.success(result, HttpHeaderParser.parseCacheHeaders(response));
    }

    @Override
    protected void deliverResponse(Result response) {
        mListener.onResponse(response);
    }
}

