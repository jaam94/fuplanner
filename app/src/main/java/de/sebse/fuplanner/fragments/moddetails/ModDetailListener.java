package de.sebse.fuplanner.fragments.moddetails;

interface ModDetailListener {
    void gotoFragmentPart(int section, int index);
}
