package de.sebse.fuplanner.fragments.canteen;

import de.sebse.fuplanner.services.canteen.types.Canteen;
import de.sebse.fuplanner.tools.network.NetworkCallback;
import de.sebse.fuplanner.tools.network.NetworkErrorCallback;

interface DaySwitcherListener {
    void onChildRefresh(NetworkCallback<Canteen> callback, NetworkErrorCallback errorCallback);
}
