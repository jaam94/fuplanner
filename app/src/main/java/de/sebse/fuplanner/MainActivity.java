package de.sebse.fuplanner;

import android.accounts.Account;
import android.accounts.AccountManager;
import android.content.ActivityNotFoundException;
import android.content.ComponentName;
import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.net.Uri;
import android.os.Bundle;
import android.os.IBinder;
import android.text.TextUtils;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.NetworkResponse;
import com.google.android.material.navigation.NavigationView;

import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Locale;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.StringRes;
import androidx.appcompat.app.AppCompatDelegate;
import androidx.arch.core.util.Function;
import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import de.sebse.fuplanner.fragments.CanteensFragment;
import de.sebse.fuplanner.fragments.ModulesFragment;
import de.sebse.fuplanner.fragments.NewsFragment;
import de.sebse.fuplanner.fragments.PrefsFragment;
import de.sebse.fuplanner.fragments.ScheduleFragment;
import de.sebse.fuplanner.fragments.StartupFragment;
import de.sebse.fuplanner.fragments.canteen.CanteensAddFragment;
import de.sebse.fuplanner.fragments.canteen.DaySwitcherFragment;
import de.sebse.fuplanner.fragments.moddetails.ModDetailFragment;
import de.sebse.fuplanner.services.canteen.CanteenBrowser;
import de.sebse.fuplanner.services.canteen.types.Canteen;
import de.sebse.fuplanner.services.canteen.types.CanteenListener;
import de.sebse.fuplanner.services.fulogin.AccountGeneral;
import de.sebse.fuplanner.services.kvv.KVV;
import de.sebse.fuplanner.services.kvv.KVVListener;
import de.sebse.fuplanner.services.kvv.Login;
import de.sebse.fuplanner.services.kvv.sync.KVVContentProvider;
import de.sebse.fuplanner.services.kvv.types.LoginTokenBB;
import de.sebse.fuplanner.services.kvv.types.LoginTokenKVV;
import de.sebse.fuplanner.services.kvv.types.Modules;
import de.sebse.fuplanner.services.news.NewsManager;
import de.sebse.fuplanner.tools.CustomAccountManager;
import de.sebse.fuplanner.tools.CustomNotificationManager;
import de.sebse.fuplanner.tools.MainActivityListener;
import de.sebse.fuplanner.tools.NewAsyncQueue;
import de.sebse.fuplanner.tools.Preferences;
import de.sebse.fuplanner.tools.Regex;
import de.sebse.fuplanner.tools.RequestPermissionsResultListener;
import de.sebse.fuplanner.tools.logging.Logger;
import de.sebse.fuplanner.tools.types.News;

public class MainActivity extends AppCompatActivity
        implements MainActivityListener, KVVListener, CanteenListener,
        NavigationView.OnNavigationItemSelectedListener,
        ModulesFragment.OnModulesFragmentInteractionListener,
        CanteensFragment.OnCanteensFragmentInteractionListener {

    public static final int FRAGMENT_NONE = -1;
    public static final int FRAGMENT_STARTUP = 0;
    public static final int FRAGMENT_MODULES = 1;
    public static final int FRAGMENT_MODULES_DETAILS = 2;
    public static final int FRAGMENT_SCHEDULE = 4;
    public static final int FRAGMENT_CANTEENS = 5;
    public static final int FRAGMENT_CANTEENS_DETAILS = 6;
    public static final int FRAGMENT_CANTEENS_ADD = 9;
    public static final int FRAGMENT_PREFERENCES = 7;
    public static final int FRAGMENT_NEWS = 8;

    private static final String ARG_FRAGMENT_PAGE = "fragment_page";
    private static final String ARG_FRAGMENT_STATUS = "fragment_status";
    private static final int DOUBLE_CLICK_TO_EXIT_MILLIS = 2000;

    private FragmentManager mFragmentManager;
    private NewsManager mNewsManager;
    private CanteenBrowser mCanteenBrowser;
    private final Logger log = new Logger(this);
    private NavigationView mNavigationView;

    private boolean mAlreadyCreated = false;

    // KVV service
    private ArrayList<KVVCallback> mCallbacks = new ArrayList<>();
    @Nullable private KVV mKVV;
    boolean mBound = false;
    /** Defines callbacks for service binding, passed to bindService() */
    private ServiceConnection mConnection = new ServiceConnection() {
        @Override
        public void onServiceConnected(ComponentName className,
                                       IBinder service) {
            // We've bound to LocalService, cast the IBinder and get LocalService instance
            KVV.LocalBinder binder = (KVV.LocalBinder) service;
            mKVV = binder.getService();
            mKVV.addListener("mainactivity", MainActivity.this);
            for (KVVCallback callback : mCallbacks) {
                callback.get(mKVV);
            }
            mBound = true;

        }

        @Override
        public void onServiceDisconnected(ComponentName arg0) {
            mKVV.removeListener("mainactivity");
            mBound = false;
            mKVV = null;
        }
    };



    private int mFragmentPage = FRAGMENT_NONE;
    @NotNull
    private String mFragmentData = "";
    private final HashMap<String, RequestPermissionsResultListener> permissionListeners = new HashMap<>();
    private final NewAsyncQueue mQueue = new NewAsyncQueue();
    private long mDoubleBackToExitPressedOnce = 0;
    private CustomAccountManager mAccountManager;
    private boolean isPaused = false;
    private boolean isLoggedInBeforePause = false;
    private boolean isDrawerFixed = false;
    private Function<Modules, Void> mMolduleListUpdateFunction;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mAccountManager = new CustomAccountManager(AccountManager.get(this), () -> MainActivity.this);
        isDrawerFixed = getResources().getBoolean(R.bool.isDrawerFixed);
        int desiredPage = getDefaultFragmentAfterLogin();
        String desiredData = "";
        Intent intent = getIntent();
        if (intent != null && CustomNotificationManager.NOTIFICATION_TYPE_NAVIGATE.equals(intent.getStringExtra(CustomNotificationManager.NOTIFICATION_INTENT))) {
            int page = intent.getIntExtra(CustomNotificationManager.NOTIFICATION_PAGE, 0);
            if (page == FRAGMENT_STARTUP || page == FRAGMENT_NONE)
                page = getDefaultFragmentAfterLogin();
            desiredPage = page;
            desiredData = intent.getStringExtra(CustomNotificationManager.NOTIFICATION_DATA);
        } else if (savedInstanceState != null) {
            desiredPage = savedInstanceState.getInt(ARG_FRAGMENT_PAGE, desiredPage);
            desiredData = savedInstanceState.getString(ARG_FRAGMENT_STATUS, desiredData);
        }

        setContentView(R.layout.activity_main);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        if (!isDrawerFixed) {
            DrawerLayout drawer = findViewById(R.id.drawer_layout);
            ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                    this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
            drawer.addDrawerListener(toggle);
            toggle.syncState();
        }

        String nightMode = Preferences.getStringArray(getApplicationContext(), R.array.pref_night_mode);
        switch (nightMode) {
            case "night": AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_YES); break;
            case "day": AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_NO); break;
            default: AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_FOLLOW_SYSTEM); break;
        }

        mNavigationView = findViewById(R.id.nav_view);
        mNavigationView.setNavigationItemSelectedListener(this);
        mFragmentManager = getSupportFragmentManager();

        //if (mAccountManager.getAccountsByType(AccountGeneral.ACCOUNT_TYPE).length == 0) {
        if (!mAccountManager.hasAccounts(AccountGeneral.ACCOUNT_TYPE)) {
            desiredPage = getDefaultFragmentAfterLogout();
            desiredData = "";
            goToLoginScreen();
            updateNavigation();
            changeFragment(desiredPage, desiredData);
        } else {
            updateNavigation();
            changeFragment(FRAGMENT_STARTUP);
            int targetPage = desiredPage;
            String targetData = desiredData;
            getKVV(kvv -> {
                kvv.account().restoreOnlineLogin(restoreResult -> {
                    updateNavigation();
                    if (restoreResult != Login.RESTORE_STATUS_INVALID_PASSWORD)
                        changeFragment(targetPage, targetData);
                    else
                        changeFragment(getDefaultFragmentAfterLogout());
                });
            });
        }

        if (!mAlreadyCreated) {
            Intent serviceIntent = new Intent(this, KVV.class);
            getApplicationContext().bindService(serviceIntent, mConnection, Context.BIND_AUTO_CREATE);
        }
        mAlreadyCreated = true;

        CustomNotificationManager.createNotificationChannel(this);
        /*getKVV(kvv -> {
            kvv.modules().list().recv(list -> {
                Modules.Module module = list.getByIndex(0);
                CustomNotificationManager.sendNotification(this, "Test", module.title, FRAGMENT_MODULES_DETAILS, module.getID()+"."+ ModulePart.getPageByPart(ModulePart.EVENT));
            }, log::e);
        });*/
    }

    private void goToLoginScreen() {
        Preferences.setBoolean(this, R.string.pref_set_auto_sync_on_startup, false);
        mAccountManager.getTokenByType(AccountGeneral.ACCOUNT_TYPE, AccountGeneral.AUTHTOKEN_TYPE_BLACKBOARD, null, null);
    }

    @Override
    protected void onStart() {
        super.onStart();
        if (!mAlreadyCreated) {
            Intent serviceIntent = new Intent(this, KVV.class);
            getApplicationContext().bindService(serviceIntent, mConnection, Context.BIND_AUTO_CREATE);
        }
        mAlreadyCreated = true;
    }

    @Override
    protected void onStop() {
        super.onStop();
        if (mKVV != null)
            mKVV.removeListener("mainactivity");
        if (mBound)
            getApplicationContext().unbindService(mConnection);
        mKVV = null;
        mBound = false;
        mAlreadyCreated = false;
    }

    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        if (CustomNotificationManager.NOTIFICATION_TYPE_NAVIGATE.equals(intent.getStringExtra(CustomNotificationManager.NOTIFICATION_INTENT))) {
            int page = intent.getIntExtra(CustomNotificationManager.NOTIFICATION_PAGE, 0);
            String data = intent.getStringExtra(CustomNotificationManager.NOTIFICATION_DATA);
            if (page == FRAGMENT_STARTUP || page == FRAGMENT_NONE)
                page = getDefaultFragmentAfterLogin();
            changeFragment(page, data);
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        isPaused = true;
        getKVV(kvv -> isLoggedInBeforePause = kvv.account().isLoggedIn());
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (isPaused) {
            getKVV(kvv -> {
                kvv.account().restoreOnlineLogin(restoreResult -> {
                    updateNavigation();
                    if (restoreResult == Login.RESTORE_STATUS_SUCCESS && !isLoggedInBeforePause) {
                        changeFragment(getDefaultFragmentAfterLogin());
                    } else if (restoreResult == Login.RESTORE_STATUS_INVALID_PASSWORD && isLoggedInBeforePause) {
                        kvv.account().logout(false);
                        changeFragment(getDefaultFragmentAfterLogout());
                    }

                    if (restoreResult == Login.RESTORE_STATUS_SUCCESS || restoreResult == Login.RELOGIN) {
                        if (!Preferences.getBoolean(this, R.string.pref_set_auto_sync_on_startup) && mAccountManager.hasAccounts(AccountGeneral.ACCOUNT_TYPE)) {
                            registerSync(true);
                            Preferences.setBoolean(this, R.string.pref_set_auto_sync_on_startup, true);
                        } else {
                            registerSync();
                        }
                    }
                });
                kvv.modules().list().reloadIfOutdated();
            });
        }
        isPaused = false;
    }

    @Override
    public void onBackPressed() {
        if (mDoubleBackToExitPressedOnce + DOUBLE_CLICK_TO_EXIT_MILLIS > System.currentTimeMillis()) {
            super.onBackPressed();
            return;
        }

        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START) && !isDrawerFixed) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            getKVV(kvv -> {
                if (mFragmentPage == FRAGMENT_MODULES_DETAILS) {
                    ModDetailFragment fragment = (ModDetailFragment) mFragmentManager.findFragmentByTag(String.valueOf(FRAGMENT_MODULES_DETAILS));
                    if (fragment != null && fragment.isVisible() && Regex.has("\\.[1-9][0-9]*", fragment.getData())) {
                        fragment.gotoFragmentPart(0, -1);
                    } else {
                        changeFragment(FRAGMENT_MODULES);
                    }
                } else if (mFragmentPage == FRAGMENT_CANTEENS_DETAILS) {
                    DaySwitcherFragment fragment = (DaySwitcherFragment) mFragmentManager.findFragmentByTag(String.valueOf(FRAGMENT_CANTEENS_DETAILS));
                    if (fragment != null && fragment.isVisible() && Regex.has("\\.[1-9][0-9]*", fragment.getData())) {
                        fragment.gotoFragmentPart(0);
                    } else {
                        changeFragment(FRAGMENT_CANTEENS);
                    }
                } else if (mFragmentPage == FRAGMENT_CANTEENS_ADD) {
                    changeFragment(FRAGMENT_CANTEENS);
                } else if (kvv.account().isLoggedIn() && mFragmentPage != getDefaultFragmentAfterLogin()) {
                    changeFragment(getDefaultFragmentAfterLogin());
                } else {
                    mDoubleBackToExitPressedOnce = System.currentTimeMillis();
                    showToast(R.string.back_to_exit);
                    //getTokenForAccountCreateIfNeeded(AccountGeneral.ACCOUNT_TYPE, AccountGeneral.AUTHTOKEN_TYPE_BLACKBOARD);
                }
            });
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        if (mFragmentPage == FRAGMENT_SCHEDULE) {
            getMenuInflater().inflate(R.menu.options_schedule, menu);
            return true;
        } else if (mFragmentPage == FRAGMENT_CANTEENS) {
            getMenuInflater().inflate(R.menu.options_canteens, menu);
            return true;
        }
        return false;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        if (mFragmentPage == FRAGMENT_SCHEDULE) {
            if (id == R.id.refresh) {
                ScheduleFragment fragment = (ScheduleFragment) mFragmentManager.findFragmentByTag(String.valueOf(FRAGMENT_SCHEDULE));
                if (fragment != null && fragment.isVisible()) {
                    fragment.invalidate(true);
                }
                return true;
            } else if (id == R.id.go_to_today) {
                ScheduleFragment fragment = (ScheduleFragment) mFragmentManager.findFragmentByTag(String.valueOf(FRAGMENT_SCHEDULE));
                if (fragment != null && fragment.isVisible()) {
                    fragment.goToToday();
                }
                return true;
            }
        } else if (mFragmentPage == FRAGMENT_CANTEENS) {
            if (id == R.id.restore) {
                CanteensFragment fragment = (CanteensFragment) mFragmentManager.findFragmentByTag(String.valueOf(FRAGMENT_CANTEENS));
                if (fragment != null && fragment.isVisible()) {
                    fragment.restoreDefaults();
                }
                return true;
            }
            if (id == R.id.delete) {
                CanteensFragment fragment = (CanteensFragment) mFragmentManager.findFragmentByTag(String.valueOf(FRAGMENT_CANTEENS));
                if (fragment != null && fragment.isVisible()) {
                    fragment.startDelete();
                }
                return true;
            }
            if (id == R.id.add) {
                changeFragment(FRAGMENT_CANTEENS_ADD);
                return true;
            }
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();
        switch (id) {
            case R.id.nav_modules:
                changeFragment(FRAGMENT_MODULES);
                break;
            case R.id.nav_schedule:
                changeFragment(FRAGMENT_SCHEDULE);
                break;
            case R.id.nav_canteens:
                changeFragment(FRAGMENT_CANTEENS);
                break;
            case R.id.nav_news:
                changeFragment(FRAGMENT_NEWS);
                break;
            case R.id.nav_settings:
                changeFragment(FRAGMENT_PREFERENCES);
                break;
            case R.id.nav_share:
                Intent sendIntent = new Intent();
                sendIntent.setAction(Intent.ACTION_SEND);
                sendIntent.putExtra(Intent.EXTRA_TEXT, getResources().getString(R.string.share_intent, getResources().getString(R.string.app_url)));
                sendIntent.setType("text/plain");
                startActivity(sendIntent);
                break;
            case R.id.nav_rate:
                Uri uri = Uri.parse("market://details?id=" + getApplicationContext().getPackageName());
                Intent viewIntent = new Intent(Intent.ACTION_VIEW, uri);
                viewIntent.addFlags(Intent.FLAG_ACTIVITY_NO_HISTORY | Intent.FLAG_ACTIVITY_NEW_DOCUMENT | Intent.FLAG_ACTIVITY_MULTIPLE_TASK);
                try {
                    startActivity(viewIntent);
                } catch (ActivityNotFoundException e) {
                    uri = Uri.parse("http://play.google.com/store/apps/details?id=" + getApplicationContext().getPackageName());
                    viewIntent = new Intent(Intent.ACTION_VIEW, uri);
                    startActivity(viewIntent);
                }
                break;
            case R.id.nav_logout:
                getKVV(kvv -> {
                    kvv.account().logout(true);
                    kvv.modules().list().delete();
                });
                break;

        }
        if (!isDrawerFixed) {
            DrawerLayout drawer = findViewById(R.id.drawer_layout);
            drawer.closeDrawer(GravityCompat.START);
        }

        int size = mNavigationView.getMenu().size();
        for (int k = 0; k < size; k++) {
            mNavigationView.getMenu().getItem(k).setChecked(mNavigationView.getMenu().getItem(k) == item);
        }
        item.setChecked(true);

        return true;
    }

    @Override
    protected void onSaveInstanceState(@NonNull Bundle savedInstanceState) {
        if (mFragmentPage != FRAGMENT_STARTUP && mFragmentPage != FRAGMENT_NONE) {
            Fragment fragment = mFragmentManager.findFragmentByTag(String.valueOf(mFragmentPage));
            savedInstanceState.putInt(ARG_FRAGMENT_PAGE, mFragmentPage);
            if (fragment instanceof ModDetailFragment) {
                savedInstanceState.putString(ARG_FRAGMENT_STATUS, ((ModDetailFragment) fragment).getData());
            } else if (fragment instanceof DaySwitcherFragment) {
                savedInstanceState.putString(ARG_FRAGMENT_STATUS, ((DaySwitcherFragment) fragment).getData());
            } else {
                savedInstanceState.putString(ARG_FRAGMENT_STATUS, mFragmentData);
            }
        }
        super.onSaveInstanceState(savedInstanceState);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        for (RequestPermissionsResultListener listener: permissionListeners.values()) {
            listener.callback(requestCode, permissions, grantResults);
        }
    }

    /* --------------------------------------------*/
    /* --------------------------------------------*/
    /* --------------------------------------------*/

    public NewsManager getNewsManager() {
        if (this.mNewsManager == null) {
            this.mNewsManager = new NewsManager(this);
        }
        return this.mNewsManager;
    }

    public void getKVV(KVVCallback callback) {
        if (this.mKVV == null) {
            mCallbacks.add(callback);
        } else {
            callback.get(this.mKVV);
        }
    }

    public CanteenBrowser getCanteenBrowser() {
        if (this.mCanteenBrowser == null) {
            this.mCanteenBrowser = new CanteenBrowser(this);
        }
        return this.mCanteenBrowser;
    }

    private int getDefaultFragmentAfterLogin() {
        return FRAGMENT_MODULES;
    }

    private int getDefaultFragmentAfterLogout() {
        return FRAGMENT_CANTEENS;
    }

    private void toLogoutState() {
        setRefreshFailedBanner(false);
        updateNavigation();
        changeFragment(getDefaultFragmentAfterLogout());
        //mAccountManager.getTokenByType(AccountGeneral.ACCOUNT_TYPE, AccountGeneral.AUTHTOKEN_TYPE_BLACKBOARD, null, null);
    }

    private void toLoginState(String fullName, String email, int newFragment) {
        updateNavigation();

        View header = mNavigationView.getHeaderView(0);
        ((TextView) header.findViewById(R.id.login_name)).setText(fullName);
        ((TextView) header.findViewById(R.id.login_mail)).setText(email);

        changeFragment(newFragment);
    }

    private void registerSync() {
        registerSync(false);
    }

    private void registerSync(boolean isLogin) {
        Account accountByType = mAccountManager.getAccountByType(AccountGeneral.ACCOUNT_TYPE);
        if (accountByType != null) {
            if (isLogin || (ContentResolver.getMasterSyncAutomatically() && ContentResolver.getSyncAutomatically(accountByType, KVVContentProvider.PROVIDER_NAME))) {
                ContentResolver.setSyncAutomatically(accountByType, KVVContentProvider.PROVIDER_NAME, true);
            }
            ContentResolver.addPeriodicSync(
                    accountByType,
                    KVVContentProvider.PROVIDER_NAME,
                    Bundle.EMPTY,
                    Long.parseLong(Preferences.getStringArray(this, R.array.pref_sync_frequency)) * 60 * 60
            );
            forceSync();
        }
    }

    public void forceSync() {
        Account accountByType = mAccountManager.getAccountByType(AccountGeneral.ACCOUNT_TYPE);
        if (accountByType != null && ContentResolver.getMasterSyncAutomatically() && ContentResolver.getSyncAutomatically(accountByType, KVVContentProvider.PROVIDER_NAME)) {
            ContentResolver.requestSync(accountByType, KVVContentProvider.PROVIDER_NAME, Bundle.EMPTY);
        }
    }

    private void changeFragment(int newFragment) {
        changeFragment(newFragment, "");
    }

    private void changeFragment(int newFragment, String newData) {
        if (mFragmentManager.isStateSaved())
            return;
        if (newFragment == FRAGMENT_CANTEENS_DETAILS && newData.equals(""))
            newFragment = FRAGMENT_CANTEENS;
        if (newFragment == FRAGMENT_MODULES_DETAILS && newData.equals(""))
            newFragment = FRAGMENT_MODULES;

        onTitleTextChange(R.string.app_name);
        Fragment fragment;
        switch (newFragment) {
            case FRAGMENT_MODULES_DETAILS:
                fragment = ModDetailFragment.newInstance(newData);
                break;
            case FRAGMENT_MODULES:
                fragment = ModulesFragment.newInstance();
                break;
            case FRAGMENT_SCHEDULE:
                fragment = ScheduleFragment.newInstance();
                break;
            case FRAGMENT_CANTEENS_DETAILS:
                fragment = DaySwitcherFragment.newInstance(newData);
                break;
            case FRAGMENT_CANTEENS_ADD:
                fragment = CanteensAddFragment.newInstance();
                break;
            case FRAGMENT_CANTEENS:
                fragment = CanteensFragment.newInstance();
                break;
            case FRAGMENT_NEWS:
                Preferences.setLong(this, R.string.pref_last_visited_news, System.currentTimeMillis());
                updateNavigation();
                fragment = NewsFragment.newInstance();
                break;
            case FRAGMENT_PREFERENCES:
                fragment = PrefsFragment.newInstance();
                break;
            default:  // FRAGMENT_STARTUP
                fragment = StartupFragment.newInstance();
                break;
        }

        FragmentTransaction fragmentTransaction = mFragmentManager.beginTransaction();
        fragmentTransaction.replace(R.id.fragcontainer, fragment, String.valueOf(newFragment));
        fragmentTransaction.commit();

        if (newFragment == FRAGMENT_STARTUP) {
            findViewById(R.id.app_bar_include).setVisibility(View.GONE);
        } else {
            findViewById(R.id.app_bar_include).setVisibility(View.VISIBLE);
        }

        this.mFragmentPage = newFragment;
        this.mFragmentData = newData;

        invalidateOptionsMenu();
    }

    private void setRefreshFailedBanner(boolean refreshFailed) {
        View viewNoConnection = findViewById(R.id.no_connection_msg);
        if (refreshFailed)
            viewNoConnection.setVisibility(View.VISIBLE);
        else
            viewNoConnection.setVisibility(View.GONE);
    }

    private void setNavigationSelection() {
        MenuItem item = null;
        int dotPos = mFragmentData.indexOf(".");
        String moduleId = mFragmentData;
        if (dotPos != -1)
            moduleId = moduleId.substring(0, dotPos);
        switch (mFragmentPage) {
            case FRAGMENT_MODULES_DETAILS:
                String finalModuleId = moduleId;
                getKVV(kvv -> {
                    kvv.modules().list().find(finalModuleId, success -> {
                        int size = mNavigationView.getMenu().size();
                        //noinspection ConstantConditions
                        String title = success == null ? null : success.title;
                        for (int k = 0; k < size; k++) {
                            MenuItem menuItem = mNavigationView.getMenu().getItem(k);
                            if (menuItem.getTitle().equals(title)) {
                                menuItem.setChecked(true);
                                break;
                            }
                        }
                    }, log::e);
                });
                return;
            case FRAGMENT_MODULES:
                item = mNavigationView.getMenu().findItem(R.id.nav_modules);
                break;
            case FRAGMENT_SCHEDULE:
                item = mNavigationView.getMenu().findItem(R.id.nav_schedule);
                break;
            case FRAGMENT_CANTEENS_DETAILS:
                getCanteenBrowser().getCanteens(success -> {
                    int size = mNavigationView.getMenu().size();
                    try {
                        Canteen canteen = success.getCanteen(Integer.parseInt(mFragmentData));
                        String title = canteen == null ? null : canteen.getName();
                        for (int k = 0; k < size; k++) {
                            MenuItem menuItem = mNavigationView.getMenu().getItem(k);
                            if (menuItem.getTitle().equals(title)) {
                                menuItem.setChecked(true);
                                break;
                            }
                        }
                    } catch (NumberFormatException e) {
                        e.printStackTrace();
                    }
                }, log::e);
                return;
            case FRAGMENT_CANTEENS:
            case FRAGMENT_CANTEENS_ADD:
                item = mNavigationView.getMenu().findItem(R.id.nav_canteens);
                break;
            case FRAGMENT_NEWS:
                item = mNavigationView.getMenu().findItem(R.id.nav_news);
                break;
            case FRAGMENT_PREFERENCES:
                item = mNavigationView.getMenu().findItem(R.id.nav_settings);
                break;
            default:  // FRAGMENT_STARTUP / FRAGMENT_LOGIN
                break;
        }
        if (item != null)
            item.setChecked(true);
    }

    private void setNavigationHeader(boolean isLoggedIn) {
        View header = mNavigationView.getHeaderView(0);
        int login = isLoggedIn ? View.VISIBLE : View.GONE;
        int btn = !isLoggedIn ? View.VISIBLE : View.GONE;

        header.findViewById(R.id.login_name).setVisibility(login);
        header.findViewById(R.id.login_mail).setVisibility(login);
        View viewBtn = header.findViewById(R.id.btn_login_page);
        viewBtn.setVisibility(btn);
        if (!viewBtn.hasOnClickListeners())
            viewBtn.setOnClickListener(v -> {
                DrawerLayout drawer = findViewById(R.id.drawer_layout);
                if (drawer.isDrawerOpen(GravityCompat.START) && !isDrawerFixed) {
                    drawer.closeDrawer(GravityCompat.START);
                }
                goToLoginScreen();
            });
    }

    private void afterAnyMenuInflate(boolean isLoggedIn, Runnable done) {
        int MAX_COUNT = isLoggedIn ? 3 : 2;
        final int[] count = {0};
        if (isLoggedIn) {
            getKVV(kvv -> {
                kvv.modules().list().recv(success -> {
                    int i = 0;
                    for (Iterator<Modules.Module> it = success.latestSemesterIterator(); it.hasNext(); ) {
                        Modules.Module module = it.next();
                        MenuItem menuItem = mNavigationView.getMenu().add(Menu.NONE, Menu.NONE, 101 + i, module.title);
                        menuItem.setOnMenuItemClickListener(item -> {
                            onModulesFragmentInteraction(module.getID());
                            return false;
                        });
                        i++;
                    }
                    if (++count[0] == MAX_COUNT) done.run();
                }, error -> {
                    if (++count[0] == MAX_COUNT) done.run();
                    log.e(error);
                });
            });
        }
        getCanteenBrowser().getCanteens(success -> {
            int i = 0;
            for (Canteen canteen: success) {
                MenuItem menuItem = mNavigationView.getMenu().add(Menu.NONE, Menu.NONE, 201 + i, canteen.getName());
                menuItem.setOnMenuItemClickListener(item -> {
                    this.onCanteensFragmentInteraction(canteen.getId());
                    return false;
                });
                i++;
            }
            if (++count[0] == MAX_COUNT) done.run();
        }, error -> {
            if (++count[0] == MAX_COUNT) done.run();
            log.e(error);
        });
        getNewsManager().recv(success -> {
            long lastVisited = Preferences.getLong(this, R.string.pref_last_visited_news);
            int i = 0;
            for (News news: success) {
                if (news.getDate() > lastVisited) i++;
            }
            if (i > 0) {
                MenuItem menuItem = mNavigationView.getMenu().findItem(R.id.nav_news);
                menuItem.setIcon(R.drawable.ic_sms_failed);
                View view = View.inflate(this, R.layout.action_icon_number, null);
                TextView v = view.findViewById(R.id.number);
                v.setText(String.format(Locale.getDefault(), "%d", i));
                menuItem.setActionView(view);
            }
            if (++count[0] == MAX_COUNT) done.run();
        }, error -> {
            if (++count[0] == MAX_COUNT) done.run();
            log.e(error);
        });
    }

    private void updateNavigation() {
        mQueue.add(() -> {
            getKVV(kvv -> {
                boolean isLoggedIn = kvv.account().isLoggedIn();
                setNavigationHeader(isLoggedIn);
                mNavigationView.getMenu().clear();
                if (isLoggedIn)
                    mNavigationView.inflateMenu(R.menu.activity_main_drawer_login);
                else
                    mNavigationView.inflateMenu(R.menu.activity_main_drawer);
                afterAnyMenuInflate(isLoggedIn, () -> {
                    setNavigationSelection();
                    mQueue.next();
                });
            });
        });
    }












    @Override
    public void onModulesFragmentInteraction(final String itemID) {
        changeFragment(FRAGMENT_MODULES_DETAILS, itemID);
        setNavigationSelection();
    }

    @Override
    public void onModulesFragmentListUpdate(Function<Modules, Void> update) {
        this.mMolduleListUpdateFunction = update;
    }

    @Override
    public void onCanteensFragmentInteraction(final int itemID) {
        changeFragment(FRAGMENT_CANTEENS_DETAILS, String.valueOf(itemID));
        setNavigationSelection();
    }

    @Override
    public void onTitleTextChange(String newTitle) {
        setTitle(newTitle);
    }

    @Override
    public void onTitleTextChange(@StringRes int titleId) {
        setTitle(titleId);
    }

    @Override
    public void onCanteenRefreshCompleted(boolean isFailed) {
        if (!isFailed)
            updateNavigation();
        setRefreshFailedBanner(isFailed);
    }

    @Override
    public void addRequestPermissionsResultListener(RequestPermissionsResultListener listener, String id) {
        permissionListeners.put(id, listener);
    }

    @Override
    public void removeRequestPermissionsResultListener(String id) {
        permissionListeners.remove(id);
    }

    @Override
    public void showToast(@StringRes int msgStringRes) {
        showToast(getString(msgStringRes));
    }

    @Override
    public void showToast(String message) {
        Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
    }











    @Override
    public void onLogin(LoginTokenKVV tokenKVV, LoginTokenBB tokenBB, boolean isOnlyRefresh) {
        String fullName = "";
        if (tokenKVV != null && !TextUtils.isEmpty(tokenKVV.getFullName())) {
            fullName = tokenKVV.getFullName();
        } else if (tokenBB != null && !TextUtils.isEmpty(tokenBB.getUsername())) {
            fullName = tokenBB.getUsername();
        }

        String email = "";
        if (tokenKVV != null && !TextUtils.isEmpty(tokenKVV.getEmail()) && !"null".equals(tokenKVV.getEmail())) {
            email = tokenKVV.getEmail();
        } else if (tokenBB != null && !TextUtils.isEmpty(tokenBB.getUsername())) {
            email = tokenBB.getUsername() + "@zedat.fu-berlin.de";
        }
        toLoginState(fullName, email, getDefaultFragmentAfterLogin());
    }

    @Override
    public void onLogout() {
        toLogoutState();
    }

    @Override
    public void onModuleListChange() {
        updateNavigation();
    }

    @Override
    public void onKVVNetworkResponse(NetworkResponse error) {
        if (error != null)
            updateNavigation();
        setRefreshFailedBanner(error != null);
    }

    @Override
    public CustomAccountManager getAccountManager() {
        return mAccountManager;
    }

    public interface KVVCallback {
        void get(KVV kvv);
    }

    @Override
    public void onModuleListPartiallyUpdated(Modules modules) {
        if (mMolduleListUpdateFunction != null) {
            mMolduleListUpdateFunction.apply(modules);
        }
    }
}
